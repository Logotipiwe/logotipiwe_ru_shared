docker stop $(docker ps -a -q);
docker rm $(docker ps -a -q) -v;

service mysql stop;
service apache2 stop;

docker-compose up -d --build --force-recreate;
