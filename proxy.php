<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

//if (!$_GET['url']) die('url missing');
$url = "https://courseburg.ru/CRM/api.php?method=vk_bot_handler&api_token=its_a_secreet";

$data = json_decode(file_get_contents('php://input'), 1);

$opts = [
    "ssl"=> [
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    ],
    'http' =>
        [
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => json_encode($data)
        ]
];

$context = stream_context_create($opts);

echo file_get_contents($url, false, $context);
