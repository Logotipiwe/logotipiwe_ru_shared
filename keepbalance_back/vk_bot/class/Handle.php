<?php
class Handle
{
    public static $kb_return = [
        "one_time" => true,
        "buttons" => [],
    ];

    public static function kb_empty()
    {
        return [
            "one_time" => true,
            "buttons" => [],
        ];
    }
    public static function kb_1()
    {
        return [
            "one_time" => false,
            "buttons" => [
                [
                    [
                        'color' => 'primary',
                        'action' => [
                            'type' => 'text',
                            'label' => "+Транз",
                            'payload' => '{"text":"stage_2"}'
                        ]
                    ],
                    [
                        'color' => 'secondary',
                        'action' => [
                            'type' => 'text',
                            'label' => 'Транзакциии',
                            'payload' => '{"text":"menu_t"}'
                        ]
                    ],
                    [
                        'color' => 'primary',
                        'action' => [
                            'type' => 'text',
                            'label' => 'Счета',
                            'payload' => '{"text":"stage_6"}'
                        ]
                    ],
                    [
                        'color' => 'negative',
                        'action' => [
                            'type' => 'text',
                            'label' => 'Выйти',
                            'payload' => '{"text":"back"}'
                        ]
                    ],
                ]
            ],
        ];
    }
    public static function kb_2($types)
    {
        $types_row = array_map(function ($item){
            return [
                'color' => 'secondary',
                'action' => [
                    'type' => 'text',
                    'label' => $item['title'],
                    'payload' => '{"text":'.$item['id'].'}'
                ]
            ];
        },$types);
        return [
            "one_time" => true,
            "buttons" => [
                [$types_row[0],$types_row[2]],
                [$types_row[3]],
                [$types_row[1],$types_row[4]],
                [
                    [
                        'color' => 'negative',
                        'action' => [
                            'type' => 'text',
                            'label' => 'Назад',
                            'payload' => '{"text":"back"}'
                        ]
                    ],
                ]
            ],
        ];
    }
    public static function kb_3($wallets)
    {
        $wallets = array_slice($wallets, 0, 5); //Клава не вмещает больше пяти в строку
        $wallets_row = array_map(function ($item){
            return [
                'color' => 'secondary',
                'action' => [
                    'type' => 'text',
                    'label' => $item['title'],
                    'payload' => '{"text":'.$item['id'].'}'
                ]
            ];
        },$wallets);
        return [
            "one_time" => true,
            "buttons" => [
                $wallets_row,
                [
                    [
                        'color' => 'negative',
                        'action' => [
                            'type' => 'text',
                            'label' => 'Назад',
                            'payload' => '{"text":"back"}'
                        ]
                    ],
                ]
            ],
        ];
    }
    public static function kb_5()
    {
        return [
            "one_time" => true,
            "buttons" => [
                [
                    [
                        'color' => 'negative',
                        'action' => [
                            'type' => 'text',
                            'label' => 'Назад',
                            'payload' => '{"text":"back"}'
                        ]
                    ],
                ]
            ],
        ];
    }
    public static function kb_6($wallets)
    {
        $wallets = array_slice($wallets, 0, 5); //Клава не вмещает больше пяти в строку
        $wallets_row = array_chunk(array_map(function ($item){
            return [
                'color' => 'secondary',
                'action' => [
                    'type' => 'text',
                    'label' => $item['title'].((!$item['init_value'])?' &#9888;':''),
                    'payload' => '{"text":'.$item['id'].'}'
                ]
            ];
        },$wallets),2);
        return [
            "one_time" => true,
            "buttons" => [
                ...$wallets_row,
                [
                    [
                        'color' => 'negative',
                        'action' => [
                            'type' => 'text',
                            'label' => 'Назад',
                            'payload' => '{"text":"back"}'
                        ]
                    ],
                ]
            ],
        ];
    }
    public static function kb_7($wallet)
    {
        if(!is_null($wallet['init_value'])){
            $values_row = [
                [
                    'color' => 'secondary',
                    'action' => [
                        'type' => 'text',
                        'label' => $wallet['init_value'],
                        'payload' => '{"text":""}'
                    ]
                ],
                [
                    'color' => 'secondary',
                    'action' => [
                        'type' => 'text',
                        'label' => $wallet['value'],
                        'payload' => '{"text":""}'
                    ]
                ]
            ];
        } else {
            $values_row = [
                [
                    'color' => 'secondary',
                    'action' => [
                        'type' => 'text',
                        'label' => 'Сделать инит',
                        'payload' => '{"text":"init","id":'.$wallet['id'].'}'
                    ]
                ]
            ];
        }
        return [
            "one_time" => true,
            "buttons" => [
                [
                    [
                        'color' => 'secondary',
                        'action' => [
                            'type' => 'text',
                            'label' => $wallet['title'],
                            'payload' => '{"text":""}'
                        ]
                    ]
                ],
                $values_row,
                [
                    [
                        'color' => 'negative',
                        'action' => [
                            'type' => 'text',
                            'label' => 'Назад',
                            'payload' => '{"text":"back"}'
                        ]
                    ],
                ]
            ],
        ];
    }

    public static function stage_0(Request $request, Database $db,$enter = false)
    {
        self::$kb_return = self::kb_empty();
        $peer_id = $request->getPeerId();
        $db->query("UPDATE users SET vk_stage = 0 where vk_peer_id = $peer_id");
        if($enter == true){
            return "Введите токен из приложения для начала работы";
        }

        if($request->getMsg() === $db->query("SELECT token from users where vk_peer_id = $peer_id")->get_result()->fetch_assoc()['token'])
        {
            return self::stage_1($request,$db,1);
        }

        return "Введите корректный токен из приложения для начала работы";
    }

    public static function stage_1(Request $request, Database $db, $enter = false)
    {
        self::$kb_return = self::kb_1();
        $peer_id = $request->getPeerId();
        $db->query("UPDATE users SET vk_stage = 1 where vk_peer_id = $peer_id");
        if($enter == true){
            if($enter === 5){
                return "Транзакция оформлена успешно!";
            } else {
                return "Выберите пункт меню для взаимодействия";
            }
        }

        if($request->getMsg() === 'back'){
            return self::stage_0($request,$db,1);
        }
        if($request->getMsg() === 'stage_2'){
            return self::stage_2($request,$db,1);
        }
        if($request->getMsg() === 'stage_6'){
            return self::stage_6($request,$db,1);
        }
        return "Выберите пункт меню для взаимодействия";
    }

    public static function stage_2(Request $request, Database $db, $enter = false)
    {
        self::$kb_return = self::kb_2($request->getTypes());
        $peer_id = $request->getPeerId();
        $db->query("UPDATE users SET vk_stage = 2 where vk_peer_id = $peer_id");

        $msg = $request->getMsg();

        $ans = "Выберите тип транзакции";

        if($enter){
            return $ans;
        }

        if($msg === 'back'){
            return self::stage_1($request,$db,1);
        }

        if(is_numeric($msg) and $msg<=5){
            $db->query("UPDATE users SET vk_selected_type = $msg WHERE vk_peer_id = $peer_id");
            return self::stage_3($request,$db,1);
        }

        return $ans;
    }

    public static function stage_3(Request $request, Database $db, $enter = false)
    {
        self::$kb_return = self::kb_3($request->getWallets(1));
        $peer_id = $request->getPeerId();
        $db->query("UPDATE users SET vk_stage = 3 where vk_peer_id = $peer_id");

        $msg = $request->getMsg();
        $user_id = $request->getUser('id');

        $ans = "Выберите счет";

        if($enter){
            return $ans;
        }

        if(is_numeric($msg) and $db->query("SELECT * FROM wallets where user_id = $user_id and id = $msg")->get_result()->num_rows){
            $db->query("UPDATE users SET vk_selected_wallet = $msg where vk_peer_id = $peer_id");
            if($db->query("SELECT vk_selected_type type from users where vk_peer_id = $peer_id")
                ->get_result()->fetch_assoc()['type'] == 2){
                return self::stage_4($request,$db,1);
            }
            return self::stage_5($request,$db,1);
        }

        if($request->getMsg() === 'back'){
            return self::stage_2($request,$db,1);
        }

        return $ans;
    }

    public static function stage_4(Request $request, Database $db, $enter = false)
    {
        self::$kb_return = self::kb_3($request->getWallets(1)); //3я клавиатура такая же как 4я
        $peer_id = $request->getPeerId();
        $db->query("UPDATE users SET vk_stage = 4 where vk_peer_id = $peer_id");

        $msg = $request->getMsg();
        $user_id = $request->getUser('id');

        $ans = "Выберите счет зачисления";

        if($enter){
            return $ans;
        }
        //TODO неверно заносится в базу выбранное значение, перенести это в if($enter) и сделать проверку откуда приход
        if(is_numeric($msg) and $db->query("SELECT * FROM wallets where user_id = $user_id and id = $msg")->get_result()->num_rows){
            $db->query("UPDATE users SET vk_selected_wallet_to = $msg where vk_peer_id = $peer_id");
            return self::stage_5($request,$db,1);
        }

        if($request->getMsg() === 'back'){
            return self::stage_3($request,$db,1);
        }

        return $ans;
    }

    public static function stage_5(Request $request, Database $db, $enter = false)
    {
        self::$kb_return = self::kb_5();
        $peer_id = $request->getPeerId();
        $db->query("UPDATE users SET vk_stage = 5 where vk_peer_id = $peer_id");

        $msg = $request->getMsg();

        $ans = "Введите сумму";

        if($enter){
            return $ans;
        }

        if(is_numeric($msg)){
            $type = $request->getUser('vk_selected_type');
            $wallet = $request->getUser('vk_selected_wallet');
            $to_wallet = $request->getUser('vk_selected_wallet_to');

            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => "https://logotipiwe.ru/new_money/back/api.php",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS =>
                    "method=transaction_new" .
                    "&wallet_id= ".$wallet .
                    "&to_wallet=".$to_wallet .
                    "&type=".$type .
                    "&value=".$msg .
                    "&date=".date_create()->format('Y-m-d'),
                CURLOPT_HTTPHEADER => [
                    "Accept: application/json",
                    "Content-Type: application/x-www-form-urlencoded",
                    "Cookie: vk_token=".Config::$secretKey."; peer_id=$peer_id",
                ],
            ]);
            $response = curl_exec($curl);
            $response = json_decode($response,1);
            curl_close($curl);
            if($response['ok']){
                return self::stage_1($request,$db,5);
            } else {
                //TODO убрать этот вывод, сделать переход на 1 стадию с разными параметрами
                return json_encode($response);
            }
        }

        if($request->getMsg() === 'back'){
            return self::stage_3($request,$db,1);
        }

        return $ans;
    }

    public static function stage_6(Request $request, Database $db, $enter = false)
    {
        $wallets = $request->getWallets();
        self::$kb_return = self::kb_6($wallets);
        $peer_id = $request->getPeerId();
        $user_id = $request->getUser('id');
        $db->query("UPDATE users SET vk_stage = 6 where vk_peer_id = $peer_id");

        $msg = $request->getMsg();

        $ans = ["ВАШИ СЧЕТА:"];
        $separator = '--------------------------';
        foreach ($wallets as $wallet){
            $ans[] = $separator;
            $ans[] = $wallet['title'];
            if($wallet['init_value'])
            $ans[] = "Инит: ".number_format($wallet['init_value']).". Средств: ".number_format($wallet['value']);
        }
        $ans = implode(PHP_EOL,$ans);

        if($enter){
            return $ans;
        }

        if($request->getMsg() === 'back'){
            return self::stage_1($request,$db,1);
        }

        if(is_numeric($msg) and $db->query("SELECT * FROM wallets where user_id = $user_id and id = $msg")->get_result()->num_rows){
            return self::stage_7($request,$db,6);
        }

        return $ans;
    }

    public static function stage_7(Request $request, Database $db, $enter = false)
    {
        $peer_id = $request->getPeerId();
        $msg = $request->getMsg();
        if($enter === 6){
            $db->query("UPDATE users SET vk_selected_wallet = $msg where vk_peer_id = $peer_id");
            $sel_wallet_id = $msg;
        } else {
            $sel_wallet_id = $db->query("SELECT vk_selected_wallet wallet_id FROM users where vk_peer_id = $peer_id")
                ->get_result()->fetch_assoc()['wallet_id'];
        }
        $sel_wallet = $request->getWallets(0,$sel_wallet_id);
        self::$kb_return = self::kb_7($sel_wallet);
        $db->query("UPDATE users SET vk_stage = 7 where vk_peer_id = $peer_id");

        $ans = "Информация по счёту '$sel_wallet[title]':".PHP_EOL."Название, начальная сумма, текущая сумма";

        if($enter){
            if($enter === 6)
            return $ans;
        }

        if($msg === 'back'){
            return self::stage_6($request,$db,1);
        }

        return $ans;
    }

    public static function message_new(Request $request, Database $db)
    {
        self::log('Новое сообщение:'.PHP_EOL . $request->getData(1));

        $peer_id = $request->getPeerId();
        $user = $db->query("SELECT * FROM users where vk_peer_id = ?",'i',$peer_id)
            ->get_result()->fetch_assoc();
        $stage_num = (int) $user['vk_stage'];
        if($stage_num<0) $stage_num = 0;

        $stage_method = 'stage_'.$stage_num;

        $ans = self::$stage_method($request, $db);

        $ans = ['message'=>$ans,'keyboard'=>json_encode(self::$kb_return)];

        self::answer($ans,$request->getPeerId());

        return 'ok';
    }

    public static function answer($msg,$peer_id)
    {
        $request_params = [
            'peer_id' => $peer_id,
            'access_token' => Config::$token,
            'random_id' => time(),
            'v' => Config::$ver
        ];
        $request_params = array_merge($msg,$request_params);
        $request_url = 'https://api.vk.com/method/messages.send?' . http_build_query($request_params);
        $ans = file_get_contents($request_url);

        self::log("Запрос для ответа на сообщение: " . PHP_EOL . json_encode($request_params));
        self::log("Результат запроса: " . PHP_EOL . $ans);

        return json_decode($ans,1);

    }

    public static function log($text)
    {
        $file = fopen("log.txt", "a+");
        $write=date_create()->format('d-m H:i:s ') . $text . PHP_EOL;
        fwrite($file,$write);
        fclose($file);
    }

    public static function confirmation()
    {
        return Config::$confirmationToken;
    }

    public static function test(Request $request)
    {
        return json_encode($request->getWallets(0,94));
    }
}
// TODO обработать вход с одного вк на разных юзеров