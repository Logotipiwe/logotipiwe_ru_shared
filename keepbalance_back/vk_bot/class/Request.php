<?php
class Request
{
    private $requestData = '';
    private $user = null;
    private $types = null;
    private $wallets = null;

    public function __construct(Database $db)
    {
        $this->requestData = json_decode(file_get_contents('php://input'),1);
        if (isset($this->requestData['object']['payload'])) {
            $this->requestData['object']['payload'] = json_decode($this->requestData['object']['payload'],1);
            $this->requestData['object']['text'] = $this->requestData['object']['payload']['text'];
        }

        $peer_id = $this->getPeerId();
        $user = $db->query("SELECT * FROM users WHERE vk_peer_id = $peer_id")->get_result();

        if($user->num_rows === 1){
            $user = $user->fetch_assoc();
            $this->user['db'] = $user;

            $this->wallets = $db->query("
                SELECT wallets.*, wallets_init.value init_value,
                    wallets_init.value - coalesce(sum(
                        CASE WHEN transactions.type = 3 THEN -transactions.value
                        WHEN  transactions_to.type = 2 THEN -transactions_to.value
                        WHEN transactions.type in (1,2,5) THEN transactions.value
                        ELSE 0 END
                    ),0) value
                FROM wallets
                left join (
                    SELECT * FROM wallets_init where year=year(now()) and month = month(now())
                ) wallets_init
                on wallets.id = wallets_init.wallet_id
                left join users on wallets.user_id = users.id
                left join transactions on wallets.id = transactions.wallet_id
                left join transactions transactions_to on transactions_to.to_wallet = wallets.id
                WHERE vk_peer_id = $peer_id
                group by wallets.id
            ")->get_result()->fetch_all(1);
        }
        $this->types = $db->query("SELECT * FROM transactions_types")->get_result()->fetch_all(1);
    }

    public function getData($json = false)
    {
        if($json) return json_encode($this->requestData);

        return $this->requestData;
    }

    public function getType()
    {
        return $this->requestData['type'];
    }

    public function getPeerId()
    {
        return (int) $this->getObj()['peer_id'];
    }

    public function getMsg()
    {
        return $this->requestData['object']['text'];
    }

    public function getObj($key = null,$json=false)
    {
        if($json) return json_encode($this->requestData['object']);
        if($key) return $this->requestData['object'][$key];

        return $this->requestData['object'];
    }

    public function getUser($key = null)
    {
        if(!is_null($key)){
            if(isset($this->user[$key])){
                return $this->user[$key];
            } else {
                return $this->user['db'][$key];
            }
        }

        return $this->user;
    }

    public function getWallets($inited = false,$id = null)
    {
        $wallets = $this->wallets;
        if(is_null($wallets)) return $wallets;

        if($inited) {
            $wallets = array_filter($wallets, function ($wallet) {
                return !is_null($wallet['init_value']);
            });
        }

        if(!is_null($id)){
            $wallets = array_values(array_filter($wallets,function ($wallet) use ($id) {
                return $wallet['id'] === $id;
            }))[0];
        }
        return $wallets;
    }

    public function getTypes()
    {
        return $this->types;
    }
    public function getPayload()
    {
        return $this->getObj('payload');
    }
}