<?php
require_once 'class/Config.php';
Config::configure();
require_once 'class/Handle.php';
require_once 'class/Database.php';
$db = new Database();
require_once 'class/Request.php';
$request = new Request($db);

if ($request->getData()['secret'] === Config::$secretKey) {
    if(!method_exists('Handle',$request->getType())){
        return json_encode(['ok'=>false,'err'=>'type_err']);
    }

    echo call_user_func(['Handle',$request->getType()],$request, $db);

}
