<?php

class Methods
{
    const without_auth = ['sign_in', 'sign_up'];

    public static function say_hi($data = null, DB $db = null)
    {
        return self::ok();
    }

    public static function json($data, DB $db)
    {
        return self::ok(['wallet 1', 'wallet 2']);
    }

    public static function ok($ans = null)
    {
        if (!is_null($ans)) return json_encode(["ok" => true, 'ans' => $ans]);
        return json_encode(["ok" => true]);
    }

    public static function err($err, $err_msg = null)
    {
        if ($err_msg) return json_encode(['ok' => false, 'err' => $err, 'err_msg' => $err_msg]);
        return json_encode(['ok' => false, 'err' => $err]);
    }

    public static function log($data, $file_name)
    {
        $file = fopen("logs/$file_name.log", 'a+');
        $str = date_create('now')->format('Y-m-d H:i:s') . " $data" . PHP_EOL;
        fwrite($file, $str);
        fclose($file);
    } //ведение лога CRM

    public static function sign_up($data, DB $db)
    {
        Validating::validate([
            'login' => 'required|string|min:2|max:20|missing:users.login',
            'password' => 'required|string'
        ], $data, $db);

        if (!$db->sign_up($data['login'], $data['password'])) {
            return self::err('exists');
        }

        return self::sign_in($data, $db);
    }

    public static function sign_in($data, DB $db)
    {
        Validating::validate([
            'login' => 'required|string|min:2|max:20|exists:users.login',
            'password' => 'required|string'
        ], $data, $db);

        $token = $db->sign_in($data['login'], $data['password']);
        if (!$token) {
            return self::err('sign_in_err');
        }
        self::set_cookie_auth($data['login'], $token);
        return self::ok(['token' => $token]);
    }

    public static function log_out($data, DB $db)
    {
        self::set_cookie_auth('', '');
        return self::ok();
    }

    public static function set_cookie_auth($login, $token)
    {
        $cookie_time = time() + 3600 * 24 * 7;
        setcookie('loc_login', $login, $cookie_time, '/');
        setcookie('token', $token, $cookie_time, '/');
    }

    public static function wallet_new($data, DB $db)
    {
        Validating::validate([
            'title' => 'nullable|string|min:1|max:25',
        ], $data, $db);

        $wallet_id = $db->new_wallet($data['title']);

        if (!$wallet_id) {
            return self::err('creation_err');
        }

        return self::ok([
            'new_id' => $wallet_id
        ]);
    }

    public static function wallet_init($data, DB $db)
    {
        Validating::validate([
            'wallet_id' => 'required|integer:use_db|exists:wallets.id|belongs:wallets.id',
            'date' => 'required|date:use_db.curr',
            'value' => 'required|integer:use_db|positive'
        ], $data, $db);

        $init_id = $db->wallet_init();


        return self::ok($init_id);
    }

    public static function wallets_get($data, DB $db)
    {
        Validating::validate([
            'date' => 'required|date:use_db.curr',
            'wallet_id' => 'nullable|integer|exists:wallets.id|belongs:wallets.id'
        ], $data, $db);

        return self::ok([
            'wallets' => $db->wallets($data['wallet_id'])
        ]);
    }

    public static function wallet_del($data, DB $db)
    {
        Validating::validate([
            'wallet_id' => 'required|integer:use_db|exists:wallets.id'
        ], $data, $db);

        $affected = $db->wallet_del();

        if (!$affected) {
            return self::err('delete_err');
        }

        return self::ok();
    }

    public static function tag_new($data, DB $db)
    {
        Validating::validate([
            'tag_name' => 'required|min:2|string:use_db'
        ], $data, $db);

        return self::ok(['tag_id' => $db->tag_new($data['tag_name'])]);
    }

    public static function transaction_new($data, DB $db)
    {
        Validating::validate([
            'type' => 'required|integer:use_db|exists:transactions_types.id',
            'wallet_id' => 'required|integer:use_db|exists:wallets.id|belongs:wallets.id',
            'value' => 'required|integer:use_db|positive|min_val:1|max_val:100000000',
            'to_wallet' => 'nullable|integer:use_db|exists:wallets.id|belongs:wallets.id',
            'is_add_to_balance' => 'nullable|integer:use_db',
            'category' => 'nullable|integer:use_db|exists:categories.id',
            'tags_ids' => 'nullable|array|exists:tags.id',
            'date' => 'required|date:use_db.curr'
        ], $data, $db);

        $date = date_create_from_format('Y-m-d', $data['date']);
        //если дата сегодняшняя
        if ($date->format('Y-m-d') === date_create()->format('Y-m-d')) {
            $insert_date = null;
        } else {
            $insert_date = $date->format('Y-m-d');
        }

        $res = $db->query("
            INSERT INTO transactions (wallet_id, value, type, to_wallet,time, category, is_add_to_balance)
            VALUES (@wallet_id,@value,@type,@to_wallet,?,@category, @is_add_to_balance)
        ", 's', $insert_date);
        $trans_id = $db->last_insert_id();

        if (!$res->affected_rows) {
            return self::err("database_err");
        }

        if (isset($data['tags_ids'])) foreach ($data['tags_ids'] as $tag_id) {
            $db->query("INSERT INTO trans_tag (trans_id,tag_id) VALUES ($trans_id,$tag_id)");
        }

        return self::ok([
            'new_id' => $db->last_insert_id(),
            'wallet' => $db->wallets($data['wallet_id'])[0],
        ]);
    }


    public static function transaction_get($data, DB $db)
    {
        Validating::validate([
            'date' => 'required|date:use_db.curr',
        ], $data, $db);

        return self::ok(['transactions' => $db->transactions()]);
    }

    public static function transaction_edit($data, DB $db)
    {
        Validating::validate([
            'id' => 'required|exists:transactions.id|integer:use_db|belongs:transactions.id',
            'value' => 'required|integer:use_db|positive|max_val:100000000'
        ], $data, $db);

        $db->query("UPDATE transactions SET value = @value WHERE id = @id");
        return self::ok([
            'wallet' => $db->wallets(
                $db->query("SELECT wallet_id FROM transactions WHERE id = @id")
                    ->get_result()
                    ->fetch_assoc()['wallet_id']
            )
        ]);
    }

    public static function transaction_del($data, DB $db)
    {
        Validating::validate([
            'id' => 'required|exists:transactions.id|integer:use_db|belongs:transactions.id',
        ], $data, $db);

        $db->query("DELETE FROM transactions WHERE id = @id");
        return self::ok();
    }

    public static function change_start_day($data, DB $db)
    {
        Validating::validate([
            'val' => 'required|integer|min_val:1',
            'date' => 'required|date:use_db.curr'
        ], $data, $db);

        $val = $data['val'];

        $res = $db->query(
            "UPDATE init 
            SET start_day = ? 
            where user_id = @user_id
            and year = @curr_year and month = @curr_month", 'i', $val);

        return self::ok($res->affected_rows);

    }

    public static function data_get($data, DB $db)
    {
        Validating::validate([
            'date' => 'required|date:use_db.curr',
        ], $data, $db);

        $ans = [
            'categories' => $db->categories(),
            'transactions' => $db->transactions(true),
            'tags' => $db->tags(),
            'balances' => $db->get_balance(3),
            'wallets' => $db->wallets(null, 1),
            'transaction_types' => $db->trans_types(),
            'start_day' => $db->get_start_day(),
            'is_init' => $db->is_init(),
            'user_id' => $db->query("SELECT @user_id u")->get_result()->fetch_assoc()['u'],
        ];
        $value_sum = array_reduce(
            $ans['wallets'],
            function ($sum, $item) {
                return $sum + $item['value'];
            }, 0);
        $ans['analytics']['value_sum'] = $value_sum - $db->unaccounted_incomes();
        $ans['analytics']['value_real_left'] = $value_sum;
        $ans['analytics']['init_sum'] = array_reduce(
            $ans['wallets'],
            function ($sum, $item) {
                return $sum + $item['init'];
            }, 0);
        $ans['analytics']['stored'] = $db->stored();
        $ans['analytics']['invested'] = $db->invested();
        $ans['analytics']['per_day'] = $db->per_day();
        $ans['analytics']['month_analytics'] = $db->month_analytics();
        return self::ok($ans);
    }
}
/**
 * TODO скролл транзакций с компа
 * TODO проверить фильтрацию по году в PHP
 * TODO сделать класс Request и пхнуть в него валидацию
 * TODO активные и не активные счета
 * TODO ежемесячные траты, подумать
 *
 * FIXME не удалять счет тупо по крестику
 * FIXME менять название счета и размер транзакции
 * FIXME разделить валидацию и внесение переменных в сессию бд
 * FIXME инкапсулировать запросы в класс БД и сделать query приватным методом
 * FIXME если удалить счет удаляется то что переведено с него на другие счета
 **/
