<?php

class DB
{
    public static $connection = null;

    public static function connect(){
        self::$connection = new mysqli('localhost','admin','-','log_comments');
        self::$connection->set_charset('utf8');
    }
    public static function reconnect($db_name){
        self::$connection = new mysqli('localhost','admin','-',$db_name);
        self::$connection->set_charset('utf8');
    }

    public static function checkConnection()
    {
        if(!self::$connection){
            self::connect();
        }
    }

    public static function close()
    {
        if(self::$connection){
            self::$connection->close();
            self::$connection = null;
        }
    }

    public static function squery($str,$types = null, ...$args)
    {
        self::checkConnection();
        $stmt = self::$connection->prepare($str);

        if(!$stmt) echo self::$connection->error;

        if(count($args)){
            $stmt->bind_param($types,...$args);
        }
        $stmt->execute();
//        if(!$stmt->errno) echo $stmt->error;
        return $stmt;
    }

    public static function query($str)
    {
        self::checkConnection();

        return self::$connection->query($str);
    }

    public static function multi_query($str)
    {
        self::checkConnection();

        $str = explode("; ",$str);
        foreach ($str as $query){
            self::query($query);
        }
    }
}
