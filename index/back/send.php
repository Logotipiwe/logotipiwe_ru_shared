<?php
header('Access-Control-Allow-Origin: *');

require_once 'db.php';

$text = $_GET['json'];
$positionX = rand(10, 90);
$positionY = rand(10, 90);
$r = rand(50, 255);
$g = rand(50, 255);
$b = rand(50, 255);
$color = "($r, $g, $b)";

$query = "INSERT INTO comments (`id`, `text`, `positionX`, `positionY`, `color`) VALUES (NULL, ?, ?, ?, ?);";
$res = DB::squery($query, 'siis', $text, $positionX, $positionY, $color);

