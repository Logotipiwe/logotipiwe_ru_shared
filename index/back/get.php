<?php

require_once 'db.php';
header('Access-Control-Allow-Origin: *');

$data = array();
$res = DB::query("SELECT * FROM comments");
while($row = $res->fetch_assoc()){
    $row['id'] = (int)$row['id'];
    $row['positionX'] = (int)$row['positionX'];
    $row['positionY'] = (int)$row['positionY'];
    $data[] = $row;
}

echo json_encode($data);
