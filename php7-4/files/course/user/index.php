<?php
require_once '../back/DB.php';
$db = new DB();
$user = $db->auth_user();
if (!$user) {
    header("Location: ../");
    die();
}
?>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Личный кабинет</title>
    <link href="index.css" rel="stylesheet">
</head>
<body>
<?php require_once '../header.php' ?>
<div id="main">
    <h2>Личный кабинет пользователя <?= $user['login'] ?></h2>
    <form action="../back/api.php">
        <h3>Данные пользователя</h3>
        <input type="hidden" name="method" value="user_data_set">
        <input name="name" placeholder="Имя..." value="<?= $user['name'] ?>">
        <input name="surname" placeholder="Фамилия..." value="<?= $user['surname'] ?>">
        <input name="phone" placeholder="Телефон..." value="<?= $user['phone'] ?>">
        <input type="submit" value="Сохранить">
    </form>
    <form action="../back/api.php">
        <h3>Смена пароля</h3>
        <input type="hidden" name="method" value="user_password_set">
        <input name="curr_password" placeholder="Текущий пароль...">
        <input name="password" placeholder="Новый пароль...">
        <input name="password2" placeholder="Повторите пароль...">
        <input type="submit" value="Сменить пароль">
    </form>
    <div id="orders_list">
        <?php
        $orders = $db->query(
            "SELECT orders.*
            FROM ord_reg
            LEFT JOIN orders on ord_reg.ord_id = orders.id
            WHERE user_id = ?",
            'i', $user['id']
        )->get_result();
        while ($order = $orders->fetch_assoc()) {
            $products = $db->query(
                "SELECT products.*, ord_prod.amount FROM ord_prod
                LEFT JOIN products on ord_prod.prod_id = products.id 
                WHERE ord_id = ?", 's', $order['id'])->get_result()->fetch_all(1);
            $products_html = array_map(function ($p) use ($order) {
                return "<div class='prod_item'>$p[name] ($p[amount] шт.)</div>";
            }, $products);

            echo "
            <div class='order_item'>
                <div class='order_item_title'>Заказ $order[id]</div>
                " . join('', $products_html) . "        
            </div>";
        }
        ?>
    </div>
</div>
</body>
</html>
