<?php
require_once "../back/DB.php";
$db = new DB();
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Корзина</title>
    <link href="index.css" rel="stylesheet">
    <script src="../libs/jquery.js"></script>
</head>
<body>
<?php require_once "../header.php"; ?>
<div id="main">
    <h1>Корзина</h1>
    <div id="cart_list"></div>
    <form id="form" action="../back/api.php">
            <input type="hidden" name="method" value="order_new">
            <input type="hidden" name="cart_str" value="" id="cart_inp">
        <?php if (!$db->auth_user()): ?>
            <h3>Введите свои данные</h3>
            <input name="name" placeholder="Имя...">
            <input name="surname" placeholder="Фамилия...">
            <input name="phone" placeholder="Телефон...">
        <?php endif ?>
    </form>
    <input type="submit" form="form" value="Оформить заказ" id="sub_btn">

</div>
<script>
	$(document).ready(() => {
		fillCart();

		$('#form').submit(e => {
			console.log('ss');
			e.preventDefault();
			localStorage.setItem('cart', '');
			e.target.submit()
		})
	});

	function fillCart() {
		let cart = false;
		try {
			cart = JSON.parse(localStorage.getItem('cart'));
		} catch (e) {}
		if (!cart) return;
		let totalSum = 0;

		$('#cart_inp').val(localStorage.getItem('cart'));
		Object.values(cart).forEach(item => {
			const itemCostSum = item.price * item.count;
			totalSum += itemCostSum;
			$('#cart_list').append(
				"<div class='cart-item'>" +
				"   <div>" + item.name + " (" + item.count + " шт.)</div>" +
				"   <div>сумма: " + itemCostSum.toLocaleString() + "p</div>" +
				"</div>"
			);
			$('#sub_btn').val('Оформить заказ (' + totalSum.toLocaleString() + 'p)');
		});
	}
</script>
</body>
</html>
