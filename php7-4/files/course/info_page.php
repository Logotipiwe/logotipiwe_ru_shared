<?php
$msg = $_GET['msg'];
$is_error = ($_GET['type'] === 'err') ? true : false;
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Информация</title>
</head>
<body>
<?php require_once 'header.php'?>
<div id="main">
    <h2><?= $msg ?></h2>
    <a href="index.php">На главную</a>
</div>
</body>
<style>
    #main{
        margin: 40px;
        flex-direction: column;
    }
</style>
</html>

