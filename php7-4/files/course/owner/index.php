<?php
require_once '../back/DB.php';
$db = new DB();
$user = $db->auth_user();
if (!$user OR $user['group'] !== 'owner') {
    header("Location: ../");
    die();
}
$income = $db->query(
        "SELECT coalesce(sum(ord_prod.price*ord_prod.amount),0) income 
        FROM ord_prod 
        LEFT JOIN orders on ord_prod.ord_id = orders.id
        WHERE status = 'Подтвержден'
        ")->get_result()->fetch_assoc()['income'];
$outcome = $db->query(
    "SELECT coalesce(sum(cost*amount),0) outcome 
        FROM admissions 
        ")->get_result()->fetch_assoc()['outcome']
?>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Личный кабинет</title>
    <link href="index.css" rel="stylesheet">
</head>
<body>
<?php require_once '../header.php' ?>
<div id="main">
    <h2>Личный кабинет владельца магазина</h2>
    <form action="../back/api.php">
        <h3>Смена пароля</h3>
        <input type="hidden" name="method" value="user_password_set">
        <input name="curr_password" placeholder="Текущий пароль...">
        <input name="password" placeholder="Новый пароль...">
        <input name="password2" placeholder="Повторите пароль...">
        <input type="submit" value="Сменить пароль">
    </form>
    <div id="owner_data">
        <div id="finance">
            <h2>Финансовые данные</h2>
            <div>
                <div>Доходы от продаж</div>
                <div class="money_value"><?=number_format($income,0,' ',' ')?></div>
            </div>
            <div>
                <div>Расходы на закупки</div>
                <div class="money_value"><?=number_format($outcome,0,' ',' ')?></div>
            </div>
            <div>
                <div>Чистая прибыль</div>
                <div class="money_value"><?=number_format($income-$outcome,0,' ',' ')?></div>
            </div>
        </div>
        <div id="rate">
            <h2>Рейтинг товаров</h2>
            <div id="rate_list">
                <?php
                $res = $db->query(
                        "SELECT 
                            products.id,
                            products.name, 
                            coalesce(sum(ord_prod.amount),0) amount, 
                            coalesce(sum(ord_prod.amount*ord_prod.price),0) price
                        FROM products 
                        LEFT JOIN ord_prod on products.id = ord_prod.prod_id
                        LEFT JOIN orders on ord_prod.ord_id = orders.id
                        WHERE orders.status = 'Подтвержден' 
                        GROUP BY products.id
                        ORDER BY 4 DESC "
                )->get_result();
                while($product = $res->fetch_assoc()){
                    echo "
                    <div class='rate_item'>$product[id]. $product[name] ($product[amount] шт.) (".number_format($product['price'])." руб.)</div>
                    ";
                }
                ?>
            </div>
        </div>
    </div>
    <div id="sellers_list">
        <h2>Спиоск продавцов</h2>
        <?php
        $res = $db->query("SELECT * FROM seller")->get_result();
        while($seller = $res->fetch_assoc()){
            echo "
            <div class='seller_item'>
                <div>$seller[id]. $seller[login]</div>
                <form action='/course/back/api.php'>
                    <input type='hidden' name='method' value='seller_data_set'>
                    <input type='hidden' name='seller_id' value='$seller[id]'>
                    <input name='name' value='$seller[name]'>
                    <input name='surname' value='$seller[surname]'>
                    <input name='pat' value='$seller[patronymic]'>
                    <input name='passport' value='$seller[passport]'>
                    <input type='submit' value='Сменить данные'>
                </form>
            </div>
            ";
        }
        ?>
        <div id="new_seller">
            <h2>Новый продавец</h2>
            <form action="/course/back/api.php">
                <input type='hidden' name='method' value='new_seller'>
                <input name='login' placeholder="Логин...">
                <input name='password' placeholder="Пароль...">
                <input name='name' placeholder="Имя...">
                <input name='surname' placeholder="Фамилия...">
                <input name='pat' placeholder="Отчество....">
                <input name='passport' placeholder="Паспорт...">
                <input type='submit' value='Создать'>
            </form>
        </div>
    </div>
</div>
</body>
</html>
