<?php
spl_autoload_register(
    function ($class_name) {
        foreach (['', 'exceptions/'] as $dir) {
            $filename = $dir . $class_name . '.php';
            if (file_exists($filename)) {
                require_once($filename);
                return;
            }
        }
    }
);

$input = json_decode(file_get_contents('php://input'), 1);
if (!is_array($input)) $input = [];
$data = array_merge($_GET, $_POST, $input);
$method = $data['method'];

$configure = new Config();
$configure->configure();

if (is_callable(['Methods', $method])) {
    $db = new DB();

    if (in_array($method, Methods::without_auth)) {
        $ans = call_user_func(['Methods', $method], $data, $db);
    } else {
        $user = $db->auth_user();

        if(!$user) {
            header("Location: ../");
            die();
        }

        $ans = call_user_func(['Methods', $method], $data, $db);
    }

    $ans = call_user_func(['Methods', $method], $data, $db);

    echo $ans;

}
