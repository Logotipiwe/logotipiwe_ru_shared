<?php

class DB
{
    protected ?mysqli $connection = null;
    public ?int $user_id;

    public function __construct()
    {
        $db_name = 'course';

        $this->connection = new mysqli('db', 'root', '---', $db_name);
        $this->connection->set_charset('utf8');
        $this->connection->query("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));");
    }

    public function query($str, $types = null, ...$args)
    {
        $stmt = $this->connection->prepare($str);

        if (!$stmt) echo $this->connection->error;

        if (count($args)) {
            $stmt->bind_param($types, ...$args);
        }
        $stmt->execute();
        if ($stmt->error AND Config::display_err) echo $stmt->error;
        return $stmt;
    }

    public function last_insert_id()
    {
        return $this->connection->insert_id;
    }

    public function auth_user($login = null, $password = null)
    {
        $is_check_password = (!is_null($login) AND !is_null($password));
        if (!$is_check_password) {
            $login = (isset($_COOKIE['login'])) ? $_COOKIE['login'] : '';
            $token_or_password = (isset($_COOKIE['token'])) ? $_COOKIE['token'] : '';

            $user_res = $this->query("SELECT * FROM users WHERE login = ?", 's', $login)->get_result();
            $seller_res = $this->query("SELECT * FROM seller WHERE login = ?", 's', $login)->get_result();
            $owner_res = $this->query("SELECT * FROM owner WHERE login = ?", 's', $login)->get_result();
        } else {
            $token_or_password = $password;
            $user_res = $this->query("SELECT * FROM users WHERE login = ? AND password = ?", 'ss', $login, md5($token_or_password))->get_result();
            $seller_res = $this->query("SELECT * FROM seller WHERE login = ? AND password = ?", 'ss', $login, md5($token_or_password))->get_result();
            $owner_res = $this->query("SELECT * FROM owner WHERE login = ? AND password = ?", 'ss', $login, md5($token_or_password))->get_result();
        }

        if ($user_res->num_rows === 1) {
            $auth = $user_res->fetch_assoc();
            $auth['group'] = 'user';
        } else if ($seller_res->num_rows === 1) {
            $auth = $seller_res->fetch_assoc();
            $auth['group'] = 'seller';
        } else if ($owner_res->num_rows === 1) {
            $auth = $owner_res->fetch_assoc();
            $auth['group'] = 'owner';
        } else {
            return false;
        }

        if (!$is_check_password) {
            $valid_token = md5($auth['login'] . $auth['password'] . $auth['rand']);
            if ($token_or_password !== $valid_token) return false;
        }

        return $auth;
    }

    public function get_product_data($product_id)
    {
        return $this->query(
            "SELECT 
                products.*,
                CAST(coalesce(sum(distinct admissions.amount), 0) as signed ) income,
                CAST(coalesce(sum(distinct ord_prod.amount),0) as signed ) outcome 
            FROM products
            LEFT JOIN admissions on products.id = admissions.product
            LEFT JOIN ord_prod on products.id = ord_prod.prod_id
            WHERE products.id = ? 
            GROUP BY products.id", 'i', $product_id
        )->get_result()->fetch_assoc();
    }
}
