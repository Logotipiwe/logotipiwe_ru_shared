<?php
class Config
{
    public const display_err = true;

    public function configure()
    {
        if(self::display_err){
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
        }
    }
}
