<?php
require_once "back/DB.php";
$db = new DB();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Магазин аудио техники</title>
    <script src="libs/jquery.js"></script>
    <link href="index.css" rel="stylesheet">
    <link href="header.css" rel="stylesheet">
</head>
<body>
<?php require_once "header.php"; ?>
<div id="main">
    <div id="search">
        <input placeholder="Поиск..."/>
    </div>
    <div id="prods">
        <div id="prods-header">
            <h2>Наши товары </h2>
        </div>
        <div id="prods-list">
            <?php
            $res = $db->query("SELECT * FROM products")->get_result();
            while ($product = $res->fetch_assoc()) {
                $product = $db->get_product_data($product['id']);
                if ($product['income'] - $product['outcome'] < 3) continue;
                echo "
                        <div 
                        class='prod-item'
                        data-price='$product[price]'
                        data-name='$product[name]'
                        data-id='$product[id]'
                        >
                            <div class='prod-item-img'><img src='img/prods/$product[id].png'></div>
                            <div class='prod-item-price'>$product[price]</div>
                            <div class='prod-item-title'>$product[name]</div>
                            <div class='prod-item-add_cart'>В корзину</div>
                        </div>                        
                        ";
            }
            ?>
        </div>
    </div>
</div>
<script>
	$(document).ready(() => {
		// setTimeout(()=>{window.location.reload()},2000)

		$('.prod-item-add_cart').click(e => {
			const prodItem = e.target.parentElement;
			const prodData = prodItem.dataset;
			const cartStr = localStorage.getItem('cart');
			let cartObj = {};
			try {
				cartObj = JSON.parse(cartStr)
			} catch (e) {
			}

			const cartNewObj = {
				id: prodData.id,
				price: prodData.price,
				name: prodData.name,
				count: 1
			};

			if (cartObj[prodData.id]) {
				if(cartObj[prodData.id].count < 3) cartObj[prodData.id].count++;
				else return alert("Вы не можете купить более 3х товаров одного вида")
			} else cartObj[prodData.id] = cartNewObj;
			localStorage.setItem('cart', JSON.stringify(cartObj));
			alert("Товар успешно добавлен в корзину")
		});
	})
</script>
</body>
</html>
