<link href="/course/header.css" rel="stylesheet">
<div id="header">
    <div id="header-title"><a href="/course">Магазин аудио техники</a></div>
    <div class="card_btn"><a href="/course/cart">Корзина</a></div>
    <?php
    require_once "back/DB.php";
    $db = new DB();
    $user = $db->auth_user();
    if(!$user):
    ?>
    <a href="/course/sign_in">Войти</a>
    <a href="/course/sign_up">Зарегистрироваться</a>
    <?php else: ?>
    <a href="/course/<?=$user['group']?>"><?=$user['login']?></a>
    <form action="/course/back/api.php">
        <input type="hidden" name="method" value="sign_out">
        <input type="submit" value="Выйти">
    </form>
    <?php endif;?>
</div>
