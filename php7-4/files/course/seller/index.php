<?php
require_once '../back/DB.php';
$db = new DB();
$user = $db->auth_user();
if (!$user) {
    header("Location: ../");
    die();
}
?>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Личный кабинет</title>
    <link href="index.css" rel="stylesheet">
</head>
<body>
<?php require_once '../header.php' ?>
<div id="main">
    <h2>Личный кабинет продавца <?= $user['login'] ?></h2>
    <form action="../back/api.php">
        <h3>Данные продавца</h3>
        <input type="hidden" name="method" value="seller_data_set">
        <input name="name" placeholder="Имя..." value="<?= $user['name'] ?>">
        <input name="surname" placeholder="Фамилия..." value="<?= $user['surname'] ?>">
        <input name="pat" placeholder="Отчество..." value="<?= $user['patronymic'] ?>">
        <input name="passport" placeholder="Номер паспорта..." value="<?= $user['passport'] ?>">
        <input type="submit" value="Сохранить">
    </form>
    <form action="../back/api.php">
        <h3>Смена пароля</h3>
        <input type="hidden" name="method" value="user_password_set">
        <input name="curr_password" placeholder="Текущий пароль...">
        <input name="password" placeholder="Новый пароль...">
        <input name="password2" placeholder="Повторите пароль...">
        <input type="submit" value="Сменить пароль">
    </form>
    <div id="lists">
        <div class="list_container">
            <h2>Список необработанных заказов</h2>
            <div id="orders_list">
                <?php
                $orders = $db->query("SELECT * FROM orders WHERE status = 'Оформлен' ORDER BY timestamp DESC")->get_result();
                while ($order = $orders->fetch_assoc()) {
                    $products = $db->query(
                        "SELECT products.*, ord_prod.amount FROM ord_prod
                LEFT JOIN products on ord_prod.prod_id = products.id 
                WHERE ord_id = ?", 's', $order['id'])->get_result()->fetch_all(1);

                    $products_html = array_map(function ($p) use ($order) {
                        return "<div class='prod_item'>$p[name] ($p[amount] шт.)</div>";
                    }, $products);

                    echo "
            <div class='order_item'>
                <div class='order_item_title'>
                    Заказ $order[id]
                    <form action='/course/back/api.php'>
                        <input type='hidden' name='method' value='order_confirm'>
                        <input type='hidden' name='order_id' value='$order[id]'>
                        <input type='submit' value='Подтвердить' class='order_item_confirm_sub'>
                    </form>
                    <form action='/course/back/api.php'>
                        <input type='hidden' name='method' value='order_cancel'>
                        <input type='hidden' name='order_id' value='$order[id]'>
                        <input type='submit' value='Отменить' class='order_item_cancel_sub'>
                    </form>
                </div>
                " . join('', $products_html) . "        
            </div>";
                }
                ?>
            </div>
        </div>
        <div class="list_container">
            <h2>Товары в магазине</h2>
            <div id="prod_list">
                <?php
                $products = $db->query("SELECT * FROM products")->get_result();
                while ($product = $products->fetch_assoc()) {
                    $product = $db->get_product_data($product['id']);
                    $total_count = $product['income'] - $product['outcome'];
                    echo "
            <div class='prod_item'>
                <div class='prod_item_title'>$product[name] ($total_count шт.)</div>
                <form action='/course/back/api.php'>
                    <input type='hidden' name='method' value='set_prod_data'>
                    <input type='hidden' name='prod_id' value='$product[id]'>
                    <input name='price' placeholder='Цена...' value='$product[price]'>
                    <input name='cost' placeholder='Стоимость при закупке...' value='$product[cost]'>
                    <input type='submit' value='Изменить данные'>
                </form>
                <form action='/course/back/api.php'>
                    <input type='hidden' name='method' value='add_admission'>
                    <input type='hidden' name='prod_id' value='$product[id]'>
                    <input name='amount' placeholder='Количество товара...'>
                    <input type='submit' value='Оформить поставку'>
                </form>
                <form action='/course/back/api.php'>
                    <input type='hidden' name='method' value='del_product'>
                    <input type='hidden' name='prod_id' value='$product[id]'>
                    <input type='submit' value='Удалить товар'>
                </form>
            </div>
            ";
                }
                ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>
