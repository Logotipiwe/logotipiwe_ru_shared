<?php
//require_once "exceptions/ValidationException.php";
//require_once "exceptions/AuthException.php";
//require_once "exceptions/NullableException.php";
//
//require_once 'Config.php';
//require_once 'db.php';
//require_once "Validating.php";
//require_once 'Methods.php';


$input = json_decode(file_get_contents('php://input'),1);
if(!is_array($input)) $input = [];
$data = array_merge($_GET, $_POST, $input);
$method = $data['method'];

if(is_callable(['Methods',$method])){
    try{
        $configure = new Config();
        $configure->configure();

        $db = new DB();

        if (!in_array($method, Methods::without_auth)) {
            $db->authUser();
        }

        echo call_user_func(['Methods',$method],$data,$db);

    } catch (ValidationException $e){

        http_response_code(400);

        echo json_encode([
            'ok'=>false,
            'err'=>'invalid',
            'field' => $e->getOptions('field'),
            'prop' => $e->getOptions('prop')
        ]);
    } catch (AuthException $e){

        http_response_code(401);

        echo json_encode([
            'ok'=>false,
            'err'=>'auth_err'
        ]);
    }
} else {
    http_response_code(400);
    echo json_encode(['ok'=>false,'err'=>'method_err','DDDDDDDDDDDDDDD'=>$data,
        'get'=>$_GET,
        'post'=>$_POST,
        'inp'=>json_decode(file_get_contents('php://input'),1)]);
}
